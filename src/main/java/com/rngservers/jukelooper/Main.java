package com.rngservers.jukelooper;

import com.rngservers.jukelooper.commands.JukeLooper;
import com.rngservers.jukelooper.data.DataManager;
import com.rngservers.jukelooper.events.Events;
import com.rngservers.jukelooper.juke.JukeManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private DataManager data;
    private JukeManager juke;

    @Override
    public void onEnable() {
        data = new DataManager(this);
        data.createDataFile();
        saveDefaultConfig();
        juke = new JukeManager(this, data);
        juke.enableAllJukeboxs();
        juke.secondTimer();

        this.getCommand("jukelooper").setExecutor(new JukeLooper(this));
        this.getServer().getPluginManager().registerEvents(new Events(juke, data), this);
    }

    @Override
    public void onDisable() {
        juke.disableAllJukeboxs();
    }
}
