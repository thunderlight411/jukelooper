package com.rngservers.jukelooper.commands;

import com.rngservers.jukelooper.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class JukeLooper implements CommandExecutor {
    private Main plugin;

    public JukeLooper(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.4";
        String author = "RandomUnknown";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GOLD + "JukeLooper " + ChatColor.GRAY + "♫♪ "
                    + ChatColor.GRAY + "v" + version);
            sender.sendMessage(
                    ChatColor.GRAY + "/jukelooper " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");
            if (sender.hasPermission("jukelooper.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/jukelooper reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1 && args[0].equals("reload")) {
            if (!sender.hasPermission("jukelooper.reload")) {
                sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "JukeLooper" + ChatColor.DARK_GRAY + "]"
                        + ChatColor.RESET + " No Permission!");
                return true;
            }
            plugin.reloadConfig();
            sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "JukeLooper" + ChatColor.DARK_GRAY + "]"
                    + ChatColor.RESET + " Reloaded config file!");
        }
        return true;
    }
}
